/*
 * Pasamos los metodos que estan en el main para esta clase
 */

package boletin173;

import static boletin173.Boletin173.APROBADO;
import java.util.Scanner;

/**
 *
 * @author findag
 */
public class Metodos {
    /*
    * Creamos un metodo chamado notasClase, con este metodo generara notas aleatoriamente.
    */
    public static void notasClase(int [] notas)
    {
         for (int i=0; i < notas.length; i++)
        {
            notas[i]= (int) Math.round((Math.random()*10));
        }
    }
    /*
    * Creamos un metodo chamado aprobadoSuspendo para saber que alumnos estan aprobados, só mostrara os alumnos aprobados
    */
    public static void aprobadoSuspenso(int [] notas, String [] nomes)
    {
        for (int i=0; i<notas.length; i++)
        {
            if (notas[i] >= APROBADO)
            {
                System.out.println("O alumno "+ nomes[i] + " esta aprobado");
            }
        }
    }
    /*
    * Creamos un metodo que nos dira o nome e a nota dun alumno en concreto.
    */
    public static void notaConcreta(int [] notas, String [] nomes)
    {
        System.out.println("Porfavor indique o nome do alumno que desexa saber a sua nota");
        Scanner teclado = new Scanner(System.in);
        String nome = teclado.next();
        for(int i = 0; i<nomes.length; i++)
        {
        if (nome.equals(nomes[i]))
        {
            
            System.out.println(nome + " ten un " + notas[i]);
        }
        }
    }
    /*
    * Danos a nota de Ana.
    */
    public static void notaAna(int [] notas)
    {
        System.out.println("Ana ten unha nota de " + notas[5]);
    }
    /*
    * Creamos un metodo que nos vai a ordenar de menor a maior as notas, este método podemolo usar en moitos casos.
    */
    public static void ordenada(int [] notas)
      {
         int aux;
         for (int i = 0; i < notas.length-1;i++)
         {
             for (int j = i+1; j < notas.length; j++ )
             {
                if(notas [i] > notas [j])
                {
                    aux = notas[i];
                    notas[i] = notas[j];
                    notas [j]=aux;
                } 
             }
         }
         
         for(int nota:notas)
         {
             System.out.println ( nota );
         }
          
          
      }
    
}
