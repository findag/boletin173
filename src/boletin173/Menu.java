/*
 * En este metodo
 */

package boletin173;

/**
 *
 * @author findag
 */
public class Menu {
    /*
    * Creamos un menu para poder interactuar con as distintas opcions.
    */
    public static void menus(int opcion, int[]notas, String [] nomes)
    {
        switch(opcion)
                {
                    case 1:
                    
                        Metodos.aprobadoSuspenso(notas, nomes);
                        break;
                    
                    case 2:
                    
                        Metodos.notaConcreta(notas, nomes);
                        break;
                    
                    case 3:
                    
                        Metodos.ordenada(notas);
                        break;
                    
                    case 4:
                    
                        Metodos.notaAna(notas);
                        break;
                    
                }
    }
    
}
