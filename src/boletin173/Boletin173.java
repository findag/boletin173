/*
 * Modifica o programa anterior e engadelle un array cos nomes dos alumnos 
 * Visualiza a nota dun alumno determinado
 * Visualiza unha lista co nome dos alumnos aprobados.
 * Fai unha lista ordenada por orden crecente de notas
 * Visualiza a nota dun alumno determinado que pides por teclado
 */

package boletin173;

import java.util.Scanner;

/**
 *
 * @author findag
 */
public class Boletin173 {

    /**
     * @param args the command line arguments
     */
    static final int APROBADO = 5;
    public static void main(String[] args)
    {
        Scanner teclado = new Scanner(System.in);
        int opcion;
        int [] notas = new int [5];
        String [] nomes = {"Carlos", "Nelson", "Pedro", "Dabid", "Ana"};
        Boletin173 obx = new Boletin173();
        obx.notasclase(notas);
        System.out.println("Hola bos dias, que quere facer?");
        System.out.println("Pulse 1 para visualizar unha lista dos aprobados e dos suspensos.\nPulse 2 para mirala nota un alumno en concreto.\nPulse 3 se quere ver unha lista ordenada por notas.\nPulse 4 para mirala nota de Ana.");
        opcion = teclado.nextInt();
        obx.menu(opcion, obx, notas, nomes);
    }
    public void notasclase(int [] notas)
    {
         for (int i=0; i < notas.length; i++)
        {
            notas[i]= (int) Math.round((Math.random()*10));
        }
    }
    /*
    * Creamos un metodo chamado aprobadoSuspendo para saber que alumnos estan aprobados, só mostrara os alumnos aprobados
    */
    public void aprobadoSuspenso(int [] notas, String [] nomes)
    {
        for (int i=0; i<notas.length; i++)
        {
            if (notas[i] >= APROBADO)
            {
                System.out.println("O alumno "+ nomes[i] + " esta aprobado");
            }
            else
            {
                Boletin173.suspensos(notas, nomes);
            }
        }
    }
    /*
    * Creamos un metodo que nos dira o nome e a nota dun alumno en concreto.
    */
    public void notaConcreta(int [] notas, String [] nomes)
    {
        System.out.println("Porfavor indique o nome do alumno que desexa saber a sua nota");
        Scanner teclado = new Scanner(System.in);
        String nome = teclado.next();
        for(int i = 0; i<nomes.length; i++)
        {
        if (nome.equals(nomes[i]))
        {
            
            System.out.println(nome + " ten un " + notas[i]);
        }
        }
    }
    /*
    * Danos a nota de Ana.
    */
    public void notaAna(int [] notas)
    {
        System.out.println("Ana ten unha nota de " + notas[5]);
    }
    /*
    * Creamos un metodo que nos dara la nota de los suspensos y sus nombres
    */
    public static void suspensos(int [] notas, String [] nomes)
    {
        for (int i = 0; i<notas.length; i++)
        {
            if(notas[i] < APROBADO)
            {
                System.out.println("O alumno " + nomes[i] + " esta suspenso con unha nota de " + notas[i]);
            }
        }
    }
    
    /*
    * Creamos un metodo que nos vai a ordenar de menor a maior as notas, este método podemolo usar en moitos casos.
    */
    public void ordenada(int [] notas, String [] nomes)
      {
         int aux;
         String auxNome;
         for (int i = 0; i < notas.length-1;i++)
         {
             for (int j = i+1; j < notas.length; j++ )
             {
                if(notas [i] > notas [j])
                {
                    aux = notas[i];
                    auxNome = nomes[i];
                    notas[i] = notas[j];
                    nomes[i] = nomes[j];
                    notas [j]= aux;
                    nomes[j] = auxNome;
                } 
             }
         }
         
         for(int i= 0; i < notas.length; i++)
         {
             
             System.out.println (nomes[i] + " ten unha nota de: " + notas[i] );
         }
          
          
      }
    /*
    * Creamos un menu para poder interactuar con as distintas opcions.
    */
    public void menu(int opcion, Boletin173 obx, int [] notas, String [] nomes)
    {
        switch(opcion)
                {
                    case 1:
                    {
                        obx.aprobadoSuspenso(notas, nomes);
                        break;
                    }
                    case 2:
                    {
                        obx.notaConcreta(notas, nomes);
                        break;
                    }
                    case 3:
                    {
                        obx.ordenada(notas, nomes);
                        break;
                    }
                    case 4:
                    {
                        obx.notaAna(notas);
                        break;
                    }
                }
    }
    
}
